//
// Created by radim on 02.05.2018.
//

#ifndef SNT_SOLUTION_H
#define SNT_SOLUTION_H

#include <vector>
#include <memory>
#include "Vehicle.h"

class Vehicle;

class Solution {
public:
    std::vector<Vehicle *> vehicles;

    double getTotalWeight();

    std::shared_ptr<Solution> clone();

    bool checkIntegrity();

    ~Solution();
};


#endif //SNT_SOLUTION_H
