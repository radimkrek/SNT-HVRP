CC=g++
CFLAGS=-std=c++14 -Wall -pedantic -O3 -lm
SOURCES=main.cpp SAnnealing.cpp Utils.cpp Vehicle.cpp Solution.cpp Node.cpp State.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=snt

all: $(EXECUTABLE)

.cpp.o:
	$(CC) $(CFLAGS) -c $< -o $@

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(EXECUTABLE)

run:
	./snt

clean:
	rm *.o snt