#ifndef SNT_UTILS_H
#define SNT_UTILS_H

#include <vector>
#include <string>
#include <sstream>
#include "Node.h"

class Node;

class Utils {
public:
    static Utils &instance();

    int vehicleCapacity = 0;

    bool fullComuputation = true;

    Node* depot;
    // TODO: Predelat na SmartPointery
    std::vector<Node *> nodes;
    std::vector<Node *> customers;
    std::vector<Node *> electricStations;
    std::vector<Node *> fuelStations;

    void read_dummy_file(std::string fileName);
    void read_file(std::string fileName);

    std::vector<std::string> explode(std::string const &s, char delim);

    static double random_0_1();

    ~Utils();
};


#endif //SNT_UTILS_H
