//
// Created by radim on 02.05.2018.
//

#include <iostream>
#include "Solution.h"
#include "Utils.h"

Solution::~Solution() {
    while(!vehicles.empty()){
        delete(vehicles.back());
        vehicles.pop_back();
    }
}

double Solution::getTotalWeight() {
    double ret = 0.0;
    for(auto vehicle : vehicles){
        ret = ret + vehicle->getWeight();
    }
    return ret;
}

std::shared_ptr<Solution> Solution::clone() {
    std::shared_ptr<Solution> solution(new Solution);

    for(auto v : this->vehicles){
        solution->vehicles.push_back(v->clone());
    }

    return solution;
}

bool Solution::checkIntegrity() {
    unsigned int customersCnt = 0;
    for(auto v : this->vehicles){
        for(auto n : v->path){
            if(n->node->type == TYPE_CUSTOMER){
                customersCnt++;
            }
        }
        if(!v->checkIntegrity()){
            return false;
        }
    }

    return customersCnt == Utils::instance().customers.size();

}
