#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "Utils.h"
#include "rapidxml.hpp"

using namespace rapidxml;

Utils &Utils::instance() {
    static Utils inst;
    return inst;
}

void Utils::read_dummy_file(std::string fileName) {
    std::ifstream inFile;

    inFile.open(fileName);
    if (inFile.is_open()) {
        std::string line;

        int line_count = 0;
        while (std::getline(inFile, line)) {
            auto parsedData = explode(line, ' ');
            auto node =  new Node(parsedData[0][0], line_count);
            for (unsigned int i = 1; i < parsedData.size(); i++) {
                node->distances.push_back(std::stod(parsedData[i]));
            }

            if(node->type == TYPE_DEPOT){
                depot = node;
            }else if(node->type == TYPE_ELECTRIC_STATION){
                electricStations.push_back(node);
            }else if(node->type == TYPE_FUEL_STATION){
                fuelStations.push_back(node);
            }else{
                customers.push_back(node);
            }
            nodes.push_back(node);
            line_count++;
        }

        std::cout << "Successfully readed " << line_count << " lines" << std::endl;
        std::cout << "Depot ID: " << depot->id << std::endl << "El. Stations: " << electricStations.size() << ", Fuel Stations: " << fuelStations.size() << std::endl;
        std::cout << "Customers: " << customers.size() << std::endl <<std::endl;
        inFile.close();
    } else {
        std::cout << "Unable to open file";
        exit(EXIT_FAILURE);
    }
}

void Utils::read_file(std::string fileName) {
    std::ifstream inFile;

    inFile.open(fileName);
    if (inFile.is_open()) {
        xml_document<> doc;
        xml_node<> *root_node;
        xml_node<> *node;

        std::vector<char> buffer((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());
        buffer.push_back('\0');
        doc.parse<0>(&buffer[0]);

        root_node = doc.first_node("instance");
        std::vector<std::pair<int, std::pair<int, int>>> nodes;

        node = root_node->first_node("network")->first_node("nodes");
        for(xml_node<> *point = node->first_node("node"); point; point = point->next_sibling()){
            char type;
            xml_attribute<> *typeAttr = point->first_attribute("type");

            switch (std::stoi(typeAttr->value())){
                case 0:
                    type = 'D';
                    break;
                default:
                    type = 'C';
            }
            typeAttr = point->first_attribute("id");
            int id = std::stoi(typeAttr->value())-1;
            Node* n = new Node(type, id);
            this->nodes.push_back(n);

            if(type == 'C'){
                this->customers.push_back(n);
            }

            int x = std::stoi(point->first_node("cx")->value());
            int y = std::stoi(point->first_node("cy")->value());

            nodes.push_back(std::pair<int, std::pair<int, int>>(id, std::pair<int,int>(x, y)));
        }

        this->vehicleCapacity = std::stoi(root_node->first_node("fleet")->first_node("vehicle_profile")->first_node("capacity")->value());

        node = root_node->first_node("requests");
        for(xml_node<> *point = node->first_node("request"); point; point = point->next_sibling()){
            xml_attribute<> *attr = point->first_attribute("node");
            int id = std::stoi(attr->value())-1;
            this->nodes[id]->demand = std::stoi(point->first_node("quantity")->value());
        }

        for(auto p1 : nodes){
            for(auto p2 : nodes){
                if(p1.first == p2.first) {
                    this->nodes[p1.first]->distances.push_back(0);
                }else{
                    int x = abs(p1.second.first - p2.second.first);
                    int y = abs(p1.second.second - p2.second.second);

                    this->nodes[p1.first]->distances.push_back(sqrt(pow(x, 2) + pow(y, 2)));
                }
            }
        }
        this->depot = this->nodes[0];
        this->fullComuputation = false;

        std::cout << "Successfully readed XML" << std::endl;
        std::cout << "Depot ID: " << depot->id << std::endl;
        std::cout << "Vehicle max route time: " << this->vehicleCapacity << std::endl;
        std::cout << "Customers: " << customers.size() << std::endl <<std::endl;

        inFile.close();
    } else {
        std::cout << "Unable to open file";
        exit(EXIT_FAILURE);
    }
}

Utils::~Utils() {
    while (!nodes.empty()) {
        delete (nodes.back());
        nodes.pop_back();
    }
}

std::vector<std::string> Utils::explode(std::string const &s, char delim) {
    std::vector<std::string> result;
    std::istringstream iss(s);

    for (std::string token; std::getline(iss, token, delim);) {
        result.push_back(std::move(token));
    }

    return result;
}

double Utils::random_0_1()
{
    return ((double)rand() / (RAND_MAX));
}
