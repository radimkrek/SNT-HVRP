//
// Created by radim on 02.05.2018.
//

#ifndef SNT_STATE_H
#define SNT_STATE_H

#include <memory>
#include "Node.h"

class Node;

class State {
private:
public:
    unsigned long getPos() const;

    void setPos(unsigned long pos);

private:
    unsigned long pos = 6666;

public:
    Node* node;

    double currentTime = 0.0;

    double currentDistance = 0.0;
    double currentElectricDistance = 0.0;
    double currentFuelDistance = 0.0;

    double currentElectricityLevel = 0.0;
    double currentFuelLevel = 0.0;

    State(Node *node);

    std::shared_ptr<State> clone();
};


#endif //SNT_STATE_H
