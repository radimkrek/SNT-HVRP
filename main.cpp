#include <iostream>
#include <unistd.h>
#include "Utils.h"
#include "SAnnealing.h"
#include "Solution.h"

using namespace std;

int main(int argc, char* argv[]) {
    int opt;
    bool h, c;
    std::string filename;

    h = c = false;

    while((opt = getopt(argc, argv, "hcf:")) != -1){
        switch(opt){
            case 'h':
                if(c){
                    std::cout << "You can set only -h or -c!!" << std::endl;
                    return EXIT_FAILURE;
                }
                h = true;
                break;
            case 'c':
                if(h){
                    std::cout << "You can set only -h or -c!!" << std::endl;
                    return EXIT_FAILURE;
                }
                c = true;
                break;
            case 'f':
                filename = std::string(optarg);
                break;
            default:
                std::cout << "Usage: -c|-h -f filename" << std::endl;
                std::cout << "-c run as CVRP benchmark" << std::endl;
                std::cout << "-h run as HVRP" << std::endl;
                return EXIT_SUCCESS;
        }
    }

    if(h == false && c == false){
        std::cout << "You have to specifiy type of task. -h or -c";
        return EXIT_FAILURE;
    }

    if(filename.empty()){
        std::cout << "You have to set input file";
        return EXIT_FAILURE;
    }

    if(h){
        Utils::instance().read_dummy_file(filename);
    }
    if(c) {
        Utils::instance().read_file(filename);
    }


    auto result = SAnnealing::run(1, 0.01, 0.9);

    cout << "RESULT:" << endl;
    cout << "Weight: " << result->getTotalWeight() << endl;
    cout << "Vehicles: " << result->vehicles.size() << endl;
    cout << "Paths:" << endl;
    for(auto v : result->vehicles){
        v->printPath();
    }

    return EXIT_SUCCESS;
}