//
// Created by radim on 02.05.2018.
//

#include "State.h"

State::State(Node *node) : node(node) {}

std::shared_ptr<State> State::clone() {
    std::shared_ptr<State> state(new State(this->node));
    state->pos = this->pos;
    state->currentFuelLevel = this->currentFuelLevel;
    state->currentElectricityLevel = this->currentElectricityLevel;
    state->currentTime = this->currentTime;
    state->currentFuelDistance = this->currentFuelDistance;
    state->currentElectricDistance = this->currentElectricDistance;
    state->currentDistance = this->currentDistance;
    return state;
}

unsigned long State::getPos() const {
    return this->pos;
}

void State::setPos(unsigned long pos) {
    this->pos = pos;
}
