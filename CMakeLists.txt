# cmake_minimum_required(VERSION <specify CMake version here>)
project(SNT)

set(CMAKE_CXX_STANDARD 14)

add_executable(SNT main.cpp SAnnealing.cpp SAnnealing.h Utils.cpp Utils.h Vehicle.cpp Vehicle.h Solution.cpp Solution.h Node.cpp Node.h State.cpp State.h)