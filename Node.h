#ifndef SNT_NODE_H
#define SNT_NODE_H

#include <vector>

#define TYPE_DEPOT 0
#define TYPE_ELECTRIC_STATION 1
#define TYPE_FUEL_STATION 2
#define TYPE_CUSTOMER 3

class Node {
public:
    int id;
    int type = TYPE_CUSTOMER;
    int demand = 0;
    std::vector<double> distances;

    Node(char type, int id);
};


#endif //SNT_NODE_H
