//
// Created by radim on 02.05.2018.
//

#include <iostream>
#include "Vehicle.h"
#include "Utils.h"
#include "State.h"
#include "TextTable.h"

int Vehicle::getElectricCapacity() const {
    return electricCapacity;
}

double Vehicle::getMaxElectricUsage() const {
    return maxElectricUsage;
}

double Vehicle::getElectricConsumption() const {
    return electricConsumption;
}

double Vehicle::getElectricCost() const {
    return electricCost;
}

int Vehicle::getFuelCapacity() const {
    return fuelCapacity;
}

double Vehicle::getFuelConsumption() const {
    return fuelConsumption;
}

double Vehicle::getFuelCost() const {
    return fuelCost;
}

int Vehicle::getMaxRouteTime() const {
    return maxRouteTime;
}

int Vehicle::getVelocity() const {
    return velocity;
}

Vehicle::Vehicle() {
    this->electricCapacity = 15;
    this->maxElectricUsage = 0.7;
    this->electricConsumption = 0.5;
    this->electricCost = 0.12;

    this->fuelCapacity = 25;
    this->fuelConsumption = 17.7;
    this->fuelCost = 4.18;

    if(Utils::instance().fullComuputation) {
        this->maxRouteTime = 11;
    }else{
        this->maxRouteTime = Utils::instance().vehicleCapacity;
    }

    this->velocity = 40;

    auto state = std::shared_ptr<State>(new State(Utils::instance().depot));
    state->setPos(0);
    state->currentElectricityLevel = this->electricCapacity * this->maxElectricUsage;
    state->currentFuelLevel = this->fuelCapacity;
    this->path.push_back(state);
    this->last_node = state;
}

double Vehicle::getWeight() {
    //TODO Otestovat
    //TODO Penalizace
    double currentCost;
    if(Utils::instance().fullComuputation) {
        double distanceToDepot = last_node->node->distances[Utils::instance().depot->id];

        currentCost =
                ((last_node->currentElectricDistance * this->getElectricConsumption()) * this->electricCost) +
                ((last_node->currentFuelDistance / this->getFuelConsumption()) * this->fuelCost);
        double electricDistanceLeft = (last_node->currentElectricityLevel / this->electricConsumption);

        if (electricDistanceLeft > 0) {
            if (electricDistanceLeft >= distanceToDepot) {
                currentCost =
                        currentCost + ((distanceToDepot * this->getElectricConsumption()) * this->getElectricCost());
                distanceToDepot = 0;
            } else {
                distanceToDepot = distanceToDepot - electricDistanceLeft;
                currentCost = currentCost +
                              ((electricDistanceLeft * this->getElectricConsumption()) * this->getElectricCost());
            }
        }

        if (distanceToDepot > 0) {
            currentCost = currentCost + ((distanceToDepot / this->getFuelConsumption()) * this->getFuelCost());
        }
    }else{
        currentCost = last_node->currentTime + this->getTravelTime(last_node->node->id, Utils::instance().depot->id);
    }

    return currentCost;
}

bool Vehicle::isNodeInPath(Node *node) {
    for (auto state : this->path) {
        if (state->node->id == node->id) {
            return true;
        }
    }
    return false;
}

double Vehicle::getTravelTime(int id1, int id2) {
    double distance = Utils::instance().nodes[id1]->distances[id2];
    return (distance / this->velocity) + Utils::instance().nodes[id1]->demand;
}

void Vehicle::printPath() {
    TextTable t('-', '|', '+');
    t.add("From");
    t.add("To");
    t.add("Type");
    if(Utils::instance().fullComuputation) {
        t.add("Electric Distance");
        t.add("Electric Cost");
        t.add("Fuel Distance");
        t.add("Fuel Cost");
    }else{
        t.add("Distance");
    }
    t.add("Travel Time");
    t.endOfRow();

    this->reEvaluatePath();

    std::shared_ptr<State> last_state = this->path[0];

    for (auto state : this->path) {
        if(state->node->id == this->path[0]->node->id){
            continue;
        }

        t.add(std::to_string(last_state->node->id));
        t.add(std::to_string(state->node->id));

        switch(state->node->type){
            case TYPE_FUEL_STATION:
                t.add("F station");

                break;
            case TYPE_ELECTRIC_STATION:
                t.add("E station");
                break;
            default:
                t.add("Customer");
        }

        if(Utils::instance().fullComuputation) {
            t.add(std::to_string(state->currentElectricDistance));
            t.add(std::to_string(
                    (state->currentElectricDistance * (this->getElectricConsumption())) * this->getElectricCost()));

            t.add(std::to_string(state->currentFuelDistance));
            t.add(std::to_string((state->currentFuelDistance / this->getFuelConsumption()) * this->getFuelCost()));
        }else{
            t.add(std::to_string(state->currentDistance));
        }
        t.add(std::to_string(state->currentTime));
        t.endOfRow();
        last_state = state;
    }

    t.add(std::to_string(last_state->node->id));
    t.add(std::to_string(this->path[0]->node->id));
    t.add("Depot");

    std::shared_ptr<State> depot = last_state->clone();
    depot->node = this->path[0]->node;

    double distanceToDepot = last_state->node->distances[depot->node->id];
    double electricDistanceLeft = (last_state->currentElectricityLevel / this->electricConsumption);

    if (electricDistanceLeft > 0){
        if(electricDistanceLeft >= distanceToDepot){
            depot->currentElectricDistance = depot->currentElectricDistance + distanceToDepot;
            distanceToDepot = 0;
        }else{
            distanceToDepot = distanceToDepot - electricDistanceLeft;
            depot->currentElectricDistance = depot->currentElectricDistance + electricDistanceLeft;
        }
    }

    if(distanceToDepot > 0){
        depot->currentFuelDistance = depot->currentFuelDistance + distanceToDepot;
    }

    depot->currentTime = depot->currentTime + this->getTravelTime(last_state->node->id, depot->node->id);

    if(Utils::instance().fullComuputation) {

        t.add(std::to_string(depot->currentElectricDistance));
        t.add(std::to_string(
                (depot->currentElectricDistance * this->getElectricConsumption()) * this->getElectricCost()));

        t.add(std::to_string(depot->currentFuelDistance));
        t.add(std::to_string((depot->currentFuelDistance / this->getFuelConsumption()) * this->getFuelCost()));
    }else{
        t.add(std::to_string(last_state->currentDistance + distanceToDepot));
    }
    t.add(std::to_string(depot->currentTime));

    t.endOfRow();

    t.setAlignment(2, TextTable::Alignment::LEFT);

    std::cout << t;

    std::cout << std::endl;
}

bool Vehicle::reEvaluatePath() {
    if (this->path.size() == 0) {
        return false;
    }

   // this->printPath();

    std::shared_ptr<State> last_state = this->path[0]; //depot
    int depotId = last_state->node->id;
    bool ret = true;
    for (auto state : this->path) {
        if (depotId == state->node->id) {//skip depot
            continue;
        }
        double distance = last_state->node->distances[state->node->id];
        double travelTime = this->getTravelTime(last_state->node->id, state->node->id);
        state->currentTime = last_state->currentTime + travelTime;
        state->currentDistance = last_state->currentDistance + distance;
        
        if(state->currentTime >= this->getMaxRouteTime()){
            return false;
        }

        if(Utils::instance().fullComuputation) {
            if (last_state->currentElectricityLevel > 0) {
                double electricDistanceLeft = (last_state->currentElectricityLevel / this->getElectricConsumption());
                //std::cout << electricDistanceLeft << " " << distance << std::endl;
                if (electricDistanceLeft <= distance) {
                    distance = distance - electricDistanceLeft;
                    state->currentElectricityLevel = 0;
                    state->currentElectricDistance = last_state->currentElectricDistance + electricDistanceLeft;
                } else {
                    state->currentElectricDistance =
                            last_state->currentElectricDistance + distance;
                    state->currentElectricityLevel =
                            (electricDistanceLeft - distance) * this->getElectricConsumption();

                    distance = 0;
                }
            } else {
                state->currentElectricDistance = last_state->currentElectricDistance;
                state->currentElectricityLevel = 0;
            }

            if (distance > 0) {
                state->currentFuelDistance = last_state->currentFuelDistance + distance;
                state->currentFuelLevel =
                        last_state->currentFuelLevel - (distance / this->getFuelConsumption());
            } else {
                state->currentFuelDistance = last_state->currentFuelDistance;
                state->currentFuelLevel = last_state->currentFuelLevel;
            }

            if (state->node->type == TYPE_ELECTRIC_STATION) {
                state->currentElectricityLevel = this->getElectricCapacity() * this->getMaxElectricUsage();
                this->stations.push_back(state);
            }

            if (state->node->type == TYPE_FUEL_STATION) {
                state->currentFuelLevel = this->getFuelCapacity();
                this->stations.push_back(state);
            }
        }
        state->setPos(last_state->getPos() + 1);

        if (Utils::instance().fullComuputation && state->currentFuelLevel <= 0) {
            ret = false;
        }

        last_state = state;
    }

    std::shared_ptr<State> last = this->path.at(this->path.size()-1);
    this->last_node = last;

    double travelTimeToDepot =
            this->last_node->currentTime + this->getTravelTime(this->last_node->node->id, Utils::instance().depot->id);
    if(travelTimeToDepot >= this->getMaxRouteTime()){
        return false;
    }

    if (ret) {
      //  std::cout << "ReEvaluate OK" <<std::endl;
        ret = this->checkIntegrity();
    }

    return ret;
}

void Vehicle::removeNode(unsigned long pos) {
    if (pos == 0) { //can't remove depot
        return;
    }

    if (this->last_node->node->id == this->path[pos]->node->id) {
        last_node = this->path[pos - 1];
    }


    std::vector<std::shared_ptr<State>>::iterator it = this->path.begin();
    std::advance(it, pos);

   /* std::cout << std::endl;
    std::cout << "Removin id: " << (*it)->node->id << std::endl;
    std::cout << "From " << (*it)->getPos() << std::endl;
    std::cout << std::endl;*/

    this->path.erase(it);


    this->reEvaluatePath();
}

bool Vehicle::addNode(Node *n, unsigned long pos) {
    std::shared_ptr<State> possible_state(new State(n));

    if (pos == 0) {
        return false;
    }


    if(this->isNodeInPath(n) && n->type == TYPE_CUSTOMER){
        return false;
    }

    auto it = this->path.begin();
    std::advance(it, pos);

    this->path.insert(it, possible_state);

    auto ret = this->reEvaluatePath();

   // std::cout << "Inserted: " << possible_state->getPos() << std::endl;

    return ret;
}

Vehicle *Vehicle::clone() {
    auto v = new Vehicle();
    for (auto s : this->path) {
        if (s->node->id == 0) { //skip the depot
            continue;
        }
        auto state = s->clone();
        v->path.push_back(state);

        if (state->node->type == TYPE_ELECTRIC_STATION || state->node->type == TYPE_FUEL_STATION) {
            v->stations.push_back(state);
        }
    }

    for (auto n : v->path) {
        if (n->node->id == this->last_node->node->id) {
            v->last_node = n->clone();
            break;
        }
    }

    v->reEvaluatePath();

    return v;
}

bool Vehicle::checkIntegrity() {
    for (auto s : this->stations) {
        if (s->node->type == TYPE_CUSTOMER) {
            return false;
        }
    }

    int customerCnt = 0;

    for (auto s : this->path){
        if (s->node->type == TYPE_CUSTOMER) {
            customerCnt++;
        }
    }

    if(customerCnt == 0){
        //std::cout << "NO customer in path" << std::endl;
        return false;
    }

    if(this->path.size() < 2){
       // std::cout << "Path TOO SHORT" << std::endl;
        return false;
    }

    return true;
}
