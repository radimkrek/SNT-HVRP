//
// Created by radim on 30.04.2018.
//

#ifndef SNT_SANNEALING_H
#define SNT_SANNEALING_H


#pragma once
#include "Solution.h"
#include "State.h"
#include "Vehicle.h"

#define NODE_PENALTY 25

class SAnnealing
{
public:
    static std::shared_ptr<Solution> run(double initial_temp, double final_temp, double t_decrese);

    static Node* getNearestCustomer(Vehicle* vehicle, std::vector<Node*> unservedCustomers);

    static std::shared_ptr<Solution> generateInitialSolution();

    static std::shared_ptr<Solution> swap(std::shared_ptr<Solution> actualSolution);
    static std::shared_ptr<Solution> insertion(std::shared_ptr<Solution> actualSolution);
    static std::shared_ptr<Solution> reverse(std::shared_ptr<Solution> actualSolution);
    static std::shared_ptr<Solution> insertStation(std::shared_ptr<Solution> actualSolution);
    static std::shared_ptr<Solution> deleteStation(std::shared_ptr<Solution> actualSolution);
};

#endif //SNT_SANNEALING_H
