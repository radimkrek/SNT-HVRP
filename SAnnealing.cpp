#include <iostream>
#include <cmath>
#include <memory>
#include "SAnnealing.h"
#include "Utils.h"
#include "State.h"
#include "Vehicle.h"
#include "Node.h"
#include "algorithm"


std::shared_ptr<Solution> SAnnealing::run(double initial_temp, double final_temp, double t_decrese) {
    bool needRestart, terminate, improved;
    double F_gBest = INTMAX_MAX, F_best, T, I, p, r, delta;
    unsigned long I_iter = 1000 * Utils::instance().customers.size();
    unsigned long N_nonImproving = 2 * Utils::instance().customers.size();
    unsigned int nonImprovingLoops;
    std::shared_ptr<Solution> X;
    std::shared_ptr<Solution> X_best;
    std::shared_ptr<Solution> X_gBest;
    std::shared_ptr<Solution> Y;
    nonImprovingLoops = 0;
    do {
        improved = false;
        needRestart = false;
        terminate = false;
        X = SAnnealing::generateInitialSolution();
        T = initial_temp;
        I = 0;
        F_best = X->getTotalWeight();
        X_best = X;

        while (!terminate && !needRestart) { //cyklime dokud nechceme skoncit nebo nechceme restartovat
            needRestart = false;
            terminate = false;
            p = Utils::random_0_1();
            if (p <= 0.2) {
                //std::cout << "SWAP" << std::endl;
                Y = SAnnealing::swap(X);
            } else if (p <= 0.4) {
                //std::cout << "INSERT" << std::endl;
                Y = SAnnealing::insertion(X);
            } else if (p <= 0.6) {
                //std::cout << "REVERSE" << std::endl;
                Y = SAnnealing::reverse(X);
            } else if (p <= 0.8) {
                //std::cout << "INSERT S" << std::endl;
                Y = SAnnealing::insertStation(X);
            } else {
                //std::cout << "DELETE S" << std::endl;
                Y = SAnnealing::deleteStation(X);
            }

            I = I + 1;
            delta = Y->getTotalWeight() - X->getTotalWeight();
            if (delta > 0) {
                r = Utils::random_0_1();
                if (r >= (T / (sqrt(T) / sqrt(delta)))) {
                    continue;
                }
            }

            X = Y;

            if (X->getTotalWeight() < F_best) {
                X_best = X;
                F_best = X->getTotalWeight();
                improved = true;
            }

            if (I < I_iter) {
               // std::cout << I << "/" << I_iter << std::endl;
                continue;
            }

            T = t_decrese * T;
            //std::cout << "Actual TEMP: " << T << std::endl;
            I = 0;
            if (!improved) {
                nonImprovingLoops++;
            } else {
                nonImprovingLoops = 0;
                improved = false;
            }

            //std::cout << "Non improving: " << nonImprovingLoops << std::endl;

            if (F_best < F_gBest) {
                F_gBest = F_best;
                X_gBest = X_best;
              // std::cout << "NEW BEST" << std::endl;
                continue;
            }

            if (nonImprovingLoops == 10) {
               // std::cout << "RESTART" << std::endl;
                needRestart = true;
            }

            if (!needRestart && (T < final_temp || nonImprovingLoops == N_nonImproving)) {
                //std::cout << "TERMINATING" << std::endl;
                terminate = true;
            }

        }
    } while (!terminate);

    return X_gBest;
}


Node *SAnnealing::getNearestCustomer(Vehicle *vehicle, std::vector<Node*> unservedCustomers) {
    std::vector<std::pair<double, int>> distances; // <distance, position>
    double distance;
    int i;
    for (auto node : unservedCustomers) {
        distance = 0;
        i = 0;

        if (vehicle->isNodeInPath(node)) {
            continue;
        }

        for (auto nid = vehicle->path.rbegin(); (i < 5) && (nid != vehicle->path.rend()); ++nid) {
            auto n = (*nid)->node;
            distance = distance + n->distances[node->id];

            i++;
        }
        distances.push_back(std::pair<double, int>(distance, node->id));
    }

    distance = distances[0].first;
    int nodeId = distances[0].second;

    for (auto node : distances) {
        if (node.first < distance) {
            distance = node.first;
            nodeId = node.second;
        }
    }
    Node *ret = Utils::instance().nodes[nodeId];
    return ret;
}

std::shared_ptr<Solution> SAnnealing::generateInitialSolution() {
    std::shared_ptr<Solution> solution(new Solution());
    auto vehicle = new Vehicle();
    solution->vehicles.push_back(vehicle);

    std::vector<Node*> unservedCustomers(Utils::instance().customers);

    while (!unservedCustomers.empty()) {
        for (auto v : solution->vehicles) { //first stage
            bool skipCar = false;
            while (!unservedCustomers.empty()) {
                if (v->last_node->currentTime >= v->getMaxRouteTime() ||
                    v->getTravelTime(v->last_node->node->id, Utils::instance().depot->id) >
                    v->getMaxRouteTime()) {
                    skipCar = true;
                    break;
                }

                Node *node;
                if (v->path.size() == 1) { //cesta obsahuje jen depot
                    node = unservedCustomers.front(); //vezmeme prvniho zakaznika v listu
                } else {
                    node = SAnnealing::getNearestCustomer(v, unservedCustomers);
                }

                double distance = v->last_node->node->distances[node->id];
                double travelTime = v->last_node->currentTime + v->getTravelTime(v->last_node->node->id, node->id);
                double travelTimeToDepot =
                        travelTime + v->getTravelTime(node->id, Utils::instance().depot->id);

                if (travelTime > v->getMaxRouteTime() || travelTimeToDepot > v->getMaxRouteTime()) {
                    break;
                }

                auto state = std::shared_ptr<State>(new State(node));
                state->currentTime = travelTime;
                state->currentDistance = v->last_node->currentDistance + distance;

                if(Utils::instance().fullComuputation) {

                    if (v->last_node->currentElectricityLevel > 0) {
                        double electricDistanceLeft = (v->last_node->currentElectricityLevel /
                                                       v->getElectricConsumption());
                        if (electricDistanceLeft <= distance) {
                            distance = distance - electricDistanceLeft;
                            state->currentElectricityLevel = 0;
                            state->currentElectricDistance =
                                    v->last_node->currentElectricDistance + electricDistanceLeft;
                        } else {
                            state->currentElectricDistance =
                                    v->last_node->currentElectricDistance + (electricDistanceLeft - distance);
                            state->currentElectricityLevel =
                                    (electricDistanceLeft - distance) * v->getElectricConsumption();
                            distance = 0;
                        }
                    } else {
                        state->currentElectricDistance = v->last_node->currentElectricDistance;
                    }

                    if (distance > 0) {
                        state->currentFuelDistance = v->last_node->currentFuelDistance + distance;
                        state->currentFuelLevel =
                                v->last_node->currentFuelLevel - (distance / v->getFuelConsumption());
                    }
                }
                state->setPos(v->last_node->getPos() + 1);
                v->path.push_back(state);
                v->last_node = state;
                unservedCustomers.erase(std::remove(unservedCustomers.begin(), unservedCustomers.end(), node),
                                                          unservedCustomers.end());
            }

            if (unservedCustomers.empty()) {
                break;
            } else if (skipCar) {
                continue;
            }
        }
        if(!unservedCustomers.empty()) {
            solution->vehicles.push_back(new Vehicle());
        }
    }

    return solution;
}

std::shared_ptr<Solution> SAnnealing::swap(std::shared_ptr<Solution> actualSolution) {
    Vehicle *v1;
    Vehicle *v2;

    auto newSolution = actualSolution->clone();

    /***   Select random vehicles   ***/
    auto vit = newSolution->vehicles.begin();
    std::advance(vit, rand() % newSolution->vehicles.size());

    v1 = (*vit);

    vit = newSolution->vehicles.begin();
    std::advance(vit, rand() % newSolution->vehicles.size());

    v2 = (*vit);

    if(v1->path.size() < 2 || v2->path.size() < 2){ // one path contains only depot
        return actualSolution;
    }

    /***   Select random nodes   ***/
    auto nit = v1->path.begin();
    std::advance(nit, (rand() % (v1->path.size() - 1)) + 1);

    std::shared_ptr<State> n1 = (*nit);

    nit = v2->path.begin();
    std::advance(nit, (rand() % (v2->path.size() - 1)) + 1);

    std::shared_ptr<State> n2 = (*nit);

    if(n1->node->id == n2->node->id){ //can not swap same node
        return actualSolution;
    }

   // std::cout << "Swaping ID: " << n1->node->id << " From " << n1->getPos() <<std::endl;
   // std::cout << "Swaping ID: " << n2->node->id << " From " << n2->getPos() <<std::endl;


    /***   Try swap nodes   ***/
    auto n1_original_pos = n1->getPos();
    auto n2_original_pos = n2->getPos();

    v1->removeNode(n1->getPos());
    v2->removeNode(n2->getPos());

    if(n1_original_pos <= n2_original_pos){
        if (v1->addNode(n2->node, n1_original_pos)) {
            if (v2->addNode(n1->node, n2_original_pos) && v1->checkIntegrity() && v2->checkIntegrity()) {
                return newSolution;
            }
        }
    }else{
        if (v2->addNode(n1->node, n2_original_pos)) {
            if (v1->addNode(n2->node, n1_original_pos) && v2->checkIntegrity() && v1->checkIntegrity()) {
                return newSolution;
            }
        }
    }


    return actualSolution;
}

std::shared_ptr<Solution> SAnnealing::insertion(std::shared_ptr<Solution> actualSolution) {
    Vehicle *v1;
    Vehicle *v2;

    auto newSolution = actualSolution->clone();

    /***   Select random vehicles   ***/
    auto vit = newSolution->vehicles.begin();
    std::advance(vit, rand() % newSolution->vehicles.size());

    v1 = (*vit);

    if (v1->path.size() <= 2) { // path contain only depot and one customer => i can not remove any node
        return actualSolution;
    }

    vit = newSolution->vehicles.begin();
    std::advance(vit, rand() % newSolution->vehicles.size());

    v2 = (*vit);

    /***   Select random node   ***/
    auto nid1 = v1->path.begin();
    std::advance(nid1, (rand() % (v1->path.size() - 1)) + 1);
    auto state = (*nid1);
    v1->removeNode(state->getPos());
    v1->reEvaluatePath();

    // add to random location
    unsigned long position = v2->path.size() > 1 ? ((rand() % (v2->path.size() - 1)) + 1) : 1;
    if(v2->addNode(state->node, position) && v1->checkIntegrity()){
        return newSolution;
    }else{
        // inseretion failed
        return actualSolution;
    }

}

std::shared_ptr<Solution> SAnnealing::reverse(std::shared_ptr<Solution> actualSolution) {
    auto newSolution = actualSolution->clone();

    /***   Select random car   ***/
    auto vit = newSolution->vehicles.begin();
    std::advance(vit, rand() % newSolution->vehicles.size());
    Vehicle *v = (*vit);

    if (v->path.size() < 4) { // not enough nodes for reversion
        return actualSolution;
    }

    /*** Select random nodes   ***/
    auto max = (rand() % (v->path.size() - 3)) + 3;
    auto min = (rand() % (max - 1)) + 1;

    auto nit1 = v->path.begin();
    std::advance(nit1, min);

    auto nit2 = v->path.begin();
    std::advance(nit2, max);

    std::vector<std::shared_ptr<State>> nodes;

    while (nit1++ != nit2) {
        nodes.push_back((*nit1));
    }

    auto clon = newSolution->clone();

    for (auto s : nodes) {
        v->removeNode(s->getPos());
    }

    auto clon1 = newSolution->clone();

    int i = 0;
    for (auto it = nodes.rbegin(); it != nodes.rend(); ++it) {
        if (!v->addNode((*it)->node, min + (i++))) {
            // inserting Node fail
            return actualSolution;
        }
    }

    return newSolution;
}

std::shared_ptr<Solution> SAnnealing::insertStation(std::shared_ptr<Solution> actualSolution) {
    if(!Utils::instance().fullComuputation){
        return actualSolution;
    }

    auto newSolution = actualSolution->clone();

    /***   Select random car   ***/
    auto vit = newSolution->vehicles.begin();
    std::advance(vit, rand() % newSolution->vehicles.size());
    Vehicle *v = (*vit);

    unsigned long pos = v->path.size() > 1 ? ((rand() % (v->path.size() - 1)) + 1) : 1;;

    int decision = rand() % 2;

    Node* s;

    if(decision == 0){
        auto it = Utils::instance().fuelStations.begin();
        std::advance(it, rand() % Utils::instance().fuelStations.size());
        s = (*it);
    }else{
        auto it = Utils::instance().electricStations.begin();
        std::advance(it, rand() % Utils::instance().electricStations.size());
        s = (*it);
    }

    if(v->addNode(s, pos)){
      /*  std::cout << "Node id: " << s->id << std::endl;
        std::cout << "Type: " << s->type << std::endl;
        std::cout << "Position: " << pos << std::endl;*/
        return newSolution;
    }

    return actualSolution;
}

std::shared_ptr<Solution> SAnnealing::deleteStation(std::shared_ptr<Solution> actualSolution) {
    if(!Utils::instance().fullComuputation){
        return actualSolution;
    }

    auto newSolution = actualSolution->clone();

    /***   Select random car   ***/
    auto vit = newSolution->vehicles.begin();
    std::advance(vit, rand() % newSolution->vehicles.size());
    Vehicle *v = (*vit);

    if(v->stations.size() == 0){
        return actualSolution;
    }

    auto it = v->stations.begin();
    std::advance(it, rand() % v->stations.size());

    std::shared_ptr<State> s = (*it);

    v->removeNode(s->getPos());

    return newSolution;
}
