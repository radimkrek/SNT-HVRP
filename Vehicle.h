//
// Created by radim on 02.05.2018.
//

#ifndef SNT_VEHICLE_H
#define SNT_VEHICLE_H

#include <vector>
#include <memory>
#include "State.h"

class State;

class Vehicle {
private:
    int electricCapacity;
    double maxElectricUsage;
    double electricConsumption;
    double electricCost;

    int fuelCapacity;
    double fuelConsumption;
    double fuelCost;

    int maxRouteTime;
    int velocity;

public:
    std::vector<std::shared_ptr<State>> stations;
    std::vector<std::shared_ptr<State>> path;
    std::shared_ptr<State> last_node;

    Vehicle();

    int getElectricCapacity() const;

    double getMaxElectricUsage() const;

    double getElectricConsumption() const;

    double getElectricCost() const;

    int getFuelCapacity() const;

    double getFuelConsumption() const;

    double getFuelCost() const;

    int getMaxRouteTime() const;

    int getVelocity() const;

    double getWeight();

    bool isNodeInPath(Node* node);

    double getTravelTime(int id1, int id2);

    void printPath();

    bool reEvaluatePath();

    void removeNode(unsigned long pos);

    bool addNode(Node* n, unsigned long pos);

    bool checkIntegrity();

    Vehicle* clone();
};


#endif //SNT_VEHICLE_H
