//
// Created by radim on 02.05.2018.
//

#include "Node.h"

Node::Node(char type, int id) : id(id){
    switch (type){
        case 'D':
            this->type = TYPE_DEPOT;
            break;
        case 'E':
            this->type = TYPE_ELECTRIC_STATION;
            break;
        case 'F':
            this->type = TYPE_FUEL_STATION;
            break;
        case 'C':
            this->type = TYPE_CUSTOMER;
            break;
        default:
            this->type = TYPE_CUSTOMER;
            break;
    }
}
